#include <iostream>
#include <string>
#include <vector>
#include <ctime>
#include <algorithm>
#include <Windows.h>

/*Famous Monthy Hall Problem, a thing to get around on my spare time*/

using namespace std;

static unsigned long x = 123456789, y = 362436069, z = 521288629;
unsigned long int Random(int seed = time(0)) {          //period 2^96-1
	unsigned long t;
	x ^= x << 16;
	x ^= x >> 5;
	x ^= x << 1;

	t = x;
	x = y;
	y = z;
	z = t ^ x ^ y;

	return z*seed;
}
template<class T>
float calculatePercentage(T& wins, T& Games) {
	return float(wins) / float(Games)*100;
}
int switchWin = 0;
int stayWin = 0;

int main()
{
	srand(Random());
	for (int trial = 0; /*trial < 100*/; ++trial) {
		string doorHas;
		vector<string> Doors = { "Goat", "Goat", "Car" };
		random_shuffle(begin(Doors), end(Doors)); // Shuffle what is in doors
		vector<string>::iterator doorNum = Doors.begin() + rand() % 3; //Randomly Select a Door
		cout << "Selected door has " << *doorNum << endl;
		Doors.erase(doorNum);//Eliminate selected door
		Doors[0] == "Goat" ? Doors.erase(Doors.begin()) : Doors.erase(Doors.begin() + 1);//Eliminate the Goat-infested door
		if (Doors[0] == "Car") {
			++switchWin;//if the only available door left has car in it switching wins
		}
		else
			++stayWin;//if not staying wins
		cout << "Percentage of winnings BY SWITCHING "<<calculatePercentage(switchWin, trial) << "%" <<endl;
		cout << "Percentage of winnings BY STAYING " << calculatePercentage(stayWin, trial) << "%" << endl;
		cout << "Number of switch wins: " << switchWin<<endl;
		cout << "Number of stay wins: " << stayWin;
		//Sleep(400);
		system("CLS");
	}
	system("PAUSE");
}
